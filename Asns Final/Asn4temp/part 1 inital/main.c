// ====== this is a sample main program
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <wchar.h>
#include <stdio.h>
#include <string.h>
#include "bst.h"

/**
 * @fn   get_response() gets the input of the user
 * @returns the appropriate answer Y or N, or emtpy space if it is an invalid character
 * */
char get_response(){
    char tempChar = ' '; // to store the temporary character to return
    char throwAwayChar = ' '; // to store the other keys when an invalid input is entered

    scanf("%c%c", &tempChar, &throwAwayChar); // scanning for tempChar and other inputs

    // if statement to return the valid input when detected
    if ((tempChar == '1' || tempChar == '2'||tempChar == '3'||tempChar == '4') && throwAwayChar == '\n'){
        return tempChar;
    }

    // remove the other potentially invalid characters
    while (throwAwayChar != '\n'){
        scanf("%c", &throwAwayChar);
    }
    //return empty string to signify invalid input
    return ' ';

}

char get_YN(){
    char tempChar = ' '; // to store the temporary character to return
    char throwAwayChar = ' '; // to store the other keys when an invalid input is entered

    scanf("%c%c", &tempChar, &throwAwayChar); // scanning for tempChar and other inputs

    // if statement to return the valid input when detected
    if ((tempChar == 'Y' || tempChar == 'N') && throwAwayChar == '\n'){
        return tempChar;
    }

    // remove the other potentially invalid characters
    while (throwAwayChar != '\n'){
        scanf("%c", &throwAwayChar);
    }
    //return empty string to signify invalid input
    return ' ';

}

char * get_file(){
    char *filenam = " ";
    scanf("%s", filenam);
    return filenam;
}


int exit_program(BStree bst){

    printf("Thanks for using Binary Tree Organizer!\n");
    bstree_free(bst);
    return 1;
}

void fileImport(BStree bst){

    printf("Please type in the location of the file you wish to import the data from:");

    char *filename = get_file();
    FILE *file;

    file=fopen (filename,"r");

    char *name;
    int *id , *data;

    if (file) {
        while (fscanf(file, "%s %d %d", name, id, data) != EOF) {
            if (name == NULL||id == NULL|| data == NULL){
                printf("Error in reading file.\n");
                break;
            }
            bstree_insert(bst, key_construct(name, *id), *data);
        }
    }
    fclose(file);
}

void inputKeys(BStree bst){
    int escape = 0;


        printf("\nPlease type in the name(string), id(integer), and the data(int) separated by spaces you wish to add in\n"
               " that order:");

        char *name, *tempString, *token;
        int id , data ;

        name = "testing";
        gets(tempString);
        //scanf(" %d %d",  &id, &data);
        token = strtok(tempString, " ");

        for (int i = 0; i < 3; i++){
            if (token != NULL && i == 0){
                name = token;
            }
            else if (token != NULL && i == 1){
                id = atoi(token);
            }
            else if (token != NULL && i == 2){
                data = atoi(token);
            }
            token = strtok(NULL, " ");
        }

        printf("%s %d %d", name, id, data);
        if (name == NULL || id == NULL || data == NULL) {
            printf("\nInvalid input.");
        }



        while (escape == 0) {
            printf("\nWould you like to enter in more values?");
            char response = get_YN();
            if (response == 'Y') {
                break;
            } else if (response == 'N') {
                escape = 1;
                break;
            } else {
                printf("\nInvalid input.");
            }
        }

        if (escape == 1){
            return;
        }

    inputKeys(bst);
}

int main(void) {
    BStree bst;
    bst = bstree_ini(1000);
    bstree_insert(bst, key_construct("Once", 1), 11);
    bstree_insert(bst, key_construct("Upon", 22), 2);
    bstree_insert(bst, key_construct("a", 3), 33);
    bstree_insert(bst, key_construct("Time", 4), 44);
    bstree_insert(bst, key_construct("is", 5), 55);
    bstree_insert(bst, key_construct("filmed", 6), 66);
    bstree_insert(bst, key_construct("in", 7), 77);
    bstree_insert(bst, key_construct("Vancouver", 8), 88);
    bstree_insert(bst, key_construct("!", 99), 9);
    bstree_insert(bst, key_construct("Once", 5), 50);
    bstree_insert(bst, key_construct("Once", 1), 10);

    printf("Welcome to Binary Tree Organizer.");
    int escape = 0;

    while (escape == 0) {
        printf("\nSelect from the following options:"
               "\n1) Would you like to insert items into the binary tree with a file?"
               "\n2) Would you like to insert items into the binary tree by typing in the values?"
               "\n3) Would you like to traverse through the tree?"
               "\n4) Would you like to exit the program?"
               "\nType in the corresponding number to the option you would like to choose:");
        char option = get_response();

        if (option == '1') {
            fileImport(bst);
        }
        else if (option == '2') {
            inputKeys(bst);
            continue;
        }
        else if (option == '3'){
            bstree_traversal(bst);
        }
        else if (option == '4'){
            escape = exit_program(bst);
        }
        else{
            printf("invalid input.\n");
        }
    }

}