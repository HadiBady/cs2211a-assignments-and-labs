#include<stdio.h>
#include<math.h>

double piHelper(long long  n, long long currentN) {
    if (currentN > n) { return 0.0;}
    else {
        if (currentN % 2 == 1) {
            return (4 / (double) (2 * currentN - 1)) + piHelper(n, currentN + 1);
        } else {
            return piHelper(n, currentN + 1) + (-1) * (4 / ((double) 2 * currentN - 1));
        }
    }
}

double piCalculation(double E){
	//calculating the value for n, in order to use recursion
	double n =(((4/E)+1)/2)-1;
	long long nNew = (int) round(n)+ 1;
	printf("%lf %lld\n", n, nNew);
	return piHelper(nNew, 1);
}

void main(){
	double tempE;
	printf("Please enter in a value for \"e\" such that the constant pi is\ncalculated.");

	scanf("%lf", &tempE);
	
	double pi = piCalculation(tempE);

	printf("\nThe value of pi with \"e\" of %lf is %lf\n", tempE, pi);

}




/*	if (currentN%2 == 1){
			return  (4/(2*currentN-1));
		}
		else {
			return - (4/(2*currentN-1));
		}
	*/

