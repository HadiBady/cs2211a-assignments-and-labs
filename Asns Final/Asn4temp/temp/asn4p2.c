#include<stdio.h>
#include<math.h>

long double piHelper(long long  n, long long currentN){
	if (currentN == 1) {
		return 4 + piHelper(n, currentN + 1);
	}


	if (currentN > n){
	/*	if (currentN%2 == 1){
			return  (4/(2*currentN-1));
		}
		else {
			return - (4/(2*currentN-1));
		}
	*/
		return 0;
	}

	else {

		if (currentN%2 ==1){
		
			return 0 + (4/(2*currentN-1)) + piHelper(n, currentN+1);
		}		
		else {		
			return	0- (4/(2*currentN-1)) +	piHelper(n, currentN+1);
		}

	}
		

}


long double piCalculation(long double E){
	//calculating the value for n
	long long n = (((4/E)+1)/2)-1;
	long long nNew = (int) round(n) + 1;	
	
	printf("%lld %lld", n, nNew);
	
	return piHelper(nNew, 1); 


}



void main(){
	long double tempE;
	printf("Please enter in a value for \"e\" such that the constant pi is\ncalculated.");
	scanf("%Lf", &tempE);
	
	long double pi = piCalculation(tempE);

	printf("\nThe value of pi with \"e\" of %Lf is %Lf\n", tempE, pi);

}

