// exp_calculator calculates the exponent in a simpler way by dividing the exponent in half,
// unlike other longer methods that essentilay multiply the base by the self by the number of
// exponents.
// Created by M. HADI BADAWI on 2018-10-07.
//


//required to obtain user input
#include <stdio.h>


// scanInDig() function takes a digit input from the user
// it returns a digit
int scanInDig(){
    int anyInput;
    scanf(" %d", &anyInput);



    return anyInput;
}

// scanInFloat() takes a float input from the user
// returns a float
float scanInFloat(){
    float anyInput;
    
	scanf(" %f", &anyInput);
	

    return anyInput;
}

// helperExpoRecur is to aid ExpoRecurFunc by performing the recurssion on itself
// @param base		this is the base
// @param exponent	this is the exponent
// @return 		returns the base, 1, or calls the function again
float helperExpoRecur(float base, int exponent) {

    // if statement checks if the exponent falls under the base cases, otherwise, it calls
    // the function recurssively until the base case is meet
    if (exponent == 1) {
        return base;
    } else if (exponent == 0) {
        return 1.0;
    } else {
        return helperExpoRecur(base, exponent / 2) * helperExpoRecur(base, exponent / 2);
    }
}

// ExpoRecurFunc is the main function that is called to perform the calculation
// @param base		this is the base
// @param exponent	this is the exponent
// @returns 		returns the result of the function helperExpoRecur
float ExpoRecurFunc(float base, int exponent, int negative){
    // if statement to catch negative exponents
  
    if (negative == 1){
	return (1 / (helperExpoRecur(base, exponent)));

    }
    // if statement checks to see if exponent is even, if it is it simiply
    // calls the helper function, otherwise, it modifies it so that our efficient
    // method works on odd exponenets
    if ((exponent  %  2) == 0){
        return helperExpoRecur(base, exponent);
    }
    else {
        return (helperExpoRecur(base, (exponent-1)) * helperExpoRecur(base, 1));
    }
}


// main is the start of the program
// this method is what requests the inputs and prints the result of the calculation.
// this method does not return anything
void main() {
    float inBase;
    int inExpo;
    int negative = 0;

    printf("Welcome to ExpCalculator.\n");

    // to get the float input of the user
    printf("Please enter a positive base:");
    inBase = scanInFloat();

    // checks to make sure that the base is positive
    if (inBase < 0){
        printf("\nThat base value is invalid. Please enter a positive base:");
        inBase = scanInFloat();
    }

    // to get the digit input of the exponent
    printf("\nPlease enter a exponent:");
    inExpo = scanInDig();

    if (inExpo < 0){
	inExpo = (inExpo * -1);
	negative = 1;
    }

    // a very complicated way to have the function be called and printed at once.
    printf("\nThe value for an exponential function which has a base of %.2f and an exponent of %d is %f.\n", inBase, inExpo, ExpoRecurFunc(inBase, inExpo, negative));

}

