// converter - the purpose of this program is to develop a converter that converts from one
// unit of measurement into another.
// this software was developed by M. Hadi Badawi

// required to enable standard I/O
#include <stdio.h>

// declaring variables that will be used for input recognition
int inDig;
float inFloat;
char option;
char randomChar;


// scanInFloat() function deals with user input for float values
// this function returns the float input
float scanInFloat(){
    float anyInput;

    scanf("%f", &anyInput);
    scanf("%c", &randomChar);

    //  checking to make sure that you don't grab the remaining input
    while (randomChar != '\n') {
        scanf("%c", &randomChar);
    }


    return anyInput;
}

// scanInChar() function deals with user input for character values
// returns the character input
char scanInChar(){
    char anyInput;
    scanf("%c", &anyInput);
    randomChar = anyInput; // only need this to initialize the variable

    // checking to make sure that the first character is not a tab key
    while (anyInput == '\t'){
        scanf("%c", &anyInput);
        randomChar = anyInput;
    }

    //  checking to make sure that you don't grab the remaining input
    while (randomChar != '\n') {
        scanf("%c", &randomChar);
    }


    return anyInput;

}

// kiloMile() function is a function that performs the convertion from kilometer(s) to mile(s) and vice versa
// this function does not return anything, but prints the result of the conversion
void kiloMile () {

    int escape = 0; // to leave the loop when a value has been entered
    float kiloMileCon; // this variable holds the calculated result from the conversion 
    const float KiloMile = 0.62; // this is the constant to convert from kilometers to miles


     for (;;) {
	// print statemetns to display the possible options
        printf("Please select one of the following options:\n");
        printf("Type \"K\" for conversion from Kilometer and Mile\n");
        printf("Type \"M\" for conversion from Mile to Kilometer\n");

	// this uses the previously defined funciton to obtain user input
        option = scanInChar();

	// switch case to deal with the possible user input
        switch (option) {

	    // this is the case if one wants to convert from kilometers to miles
            case 'K' :
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // to calculate the conversion
                kiloMileCon = inFloat * KiloMile;

                // displaying the conversion result
                printf("%f Kilometer(s) is %.2f Mile(s)\n", inFloat, kiloMileCon);
		
		        escape = 1; // to escape out of the for loop
	
		        break; //need this otherwise it also executes teh next option

	    // this is the case if one wants to convert from miles to kilometers
            case 'M' :
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // to calculate the conversion
                kiloMileCon =  inFloat / KiloMile;

                // displaying the conversion result
                printf("%f Mile(s) is %.2f Kilometer(s)\n", inFloat, kiloMileCon);

		        escape =1; // to escape out of the for loop
		
		        break; //need this otherwise it also executes teh next option


        }
	    if (escape == 1){break;}
	}
}

// meterFeet() function converters from meter(s) to foot/feet or vice versa
// function does not return anything, but prints the result
void meterFeet () {

    int escape = 0;
    float MeterFeetCon; // this variable holds the caculated value for the conversion which is displayed to user
    const float MeterFeet = 3.28; // the constant that tells us how many feet are in a meter

   
    for (;;){

	// print statemetns to display the possible options
	printf("Please select one of the following options:\n");
        printf("Type \"M\" for conversion from Meter to Feet\n");
        printf("Type \"F\" for conversion from Feet to Meter\n");

        // obtaining user input
	option = scanInChar();

	// to deal with user input
        switch (option) {

	    // case condition if one wants to convert meters to feet
            case 'M' :
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // to calculate the conversion
                MeterFeetCon =  inFloat * MeterFeet;

                // displaying the conversion result
                printf("%f Meter(s) is %.2f Foot/Feet\n", inFloat, MeterFeetCon);

		        escape = 1; // to escape out of the for loop

		        break; //need this otherwise it also executes teh next option

	    // case condition to convert from feet to meters
            case 'F':
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // to calculate the conversion
                MeterFeetCon = inFloat / MeterFeet;

                // displaying the conversion result
                printf("%f Foot/Feet is %.2f Meter(s)\n", inFloat, MeterFeetCon);

		        escape = 1; // to escape out of the for loop

		        break; //need this otherwise it also executes teh next option

        

	}

    	if (escape == 1){break;}
    }
}


// centInch() function performs conversion between centimeters and inches
void centInch () {
    int escape = 0;
    float CentInchCon; // variable that holds the conversion value which is displayed to user
    const float CentInch = 0.39; // constant that represents how many centimetres are in an inch

    for (;;) {
    printf("Please select one of the following options:\n");
    printf("Type \"C\" for conversion from Centimetre to Inch\n");
    printf("Type \"I\" for conversion from Inch to Centimetre\n");

    // to get user input
    option = scanInChar();

        // to deal with user input
        switch (option) {
	    
	    // to convert from centimeters to inches
            case 'C' :
        	printf("Please enter the value you would like to convert:\n");

        	// to obtain valid user input
        	inFloat = (scanInFloat());

		    // to calculate the conversion
            CentInchCon = inFloat * CentInch;

		    // displaying the conversion result
            printf("%f Centimetre(s) is %.2f Inch(s)\n", inFloat, CentInchCon);

		    escape = 1; // to escape out of the for loop

		    break; //need this otherwise it also executes teh next option

	// case to convert from inches to centimeters
	    case 'I' :
		    printf("Please enter the value you would like to convert:\n");

		    // to obtain valid user input
		    inFloat = (scanInFloat());

		    // to calculate the conversion
		    CentInchCon = inFloat / CentInch;

		    // displaying the conversion result
		    printf("%f Inch(s) is %.2f Centimetre(s)\n", inFloat, CentInchCon);

		    escape = 1; // to escape out of the for loop

		    break; //need this otherwise it also executes teh next option
	
    }

	if (escape == 1){ break;}
    }
}


// function celFah() is the function that converts from celsius to fahrenheit
void celFah () {

    int escape = 1;
    float celFahCon; // this holds the value that is calculated from the conversion

    for (;;){

        printf("Please select one of the following options:\n");
        printf("Type \"C\" for conversion from Celsius to Fahrenheit\n");
        printf("Type \"F\" for conversion from Fahrenheit to Celsius\n");


        option = scanInChar();

        switch (option) {

            case 'C' :
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // this calculation is more complex then previous functions, but calculates
		        // the fahrenheit temperature from celcsius input
                celFahCon = (inFloat - 32) * 5 / 9;

                // displaying the conversion result
                printf("%f Celsius is %f Fahrenheit\n", inFloat, celFahCon);

		        escape = 1; // to escape out of the for loop

		        break; //need this otherwise it also executes teh next option

            case 'F' :
                printf("Please enter the value you would like to convert:\n");

                // to obtain valid user input
                inFloat = (scanInFloat());

                // this calculation is the reverse of the celsius case, in the since
		        // that it preforms the operations in reverse
                celFahCon = (inFloat * 9 / 5) + 32;

                // displaying the conversion result
                printf("%f Fahrenheit is %f Celsius\n", inFloat, celFahCon);
		
		        escape = 1; // to escape out of the for loop

		        break; //need this otherwise it also executes teh next option

	    

	}
	if (escape == 1) {break;}
	
    }
}

// the main() function allows the user to select which conversion they would like to perform
// this method does not return any values, but does deal with invalid inputs
void main() {

    int escape = 0;

    printf("Welcome to Converter.\n");


    while (escape == 0) {

        printf("Please select one of the following options:\n");
        printf("Type \"1\" for conversion between Kilometers and Mile\n");
        printf("Type \"2\" for conversion between Meter and Feet\n");
        printf("Type \"3\" for conversion between Centimetre and Inch\n");
        printf("Type \"4\" for conversion between Celsius and Fahrenheit\n");
        printf("Type \"5\" to quit\n");

        option = scanInChar();

	// in each case, the appropriate function is called to ensure that the proper conversion is performed
       if (option == '1')
	        {kiloMile();}
       else if (option == '2')
	        { meterFeet();}
       else if (option == '3') 
	        {centInch();}
       else if (option == '4')
	        {celFah();}
	    // this case deals with the user wanting to terminate the program
       else if (option == '5')
	    {
            printf("Thanks for using Converter!\n");
       		escape = 1;
       	}
       else {	   
	        printf("I am sorry, that was an invalid selection.\n");
	    }

    }

}


