﻿#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/**
 * @author  M. Hadi Badawi
 * @details  purpose of this program is to evaluate simple arithmetic expressions using recursion.
 *
 * */



// Input: none, read from stdin
// Effect: get the next numeric value of the expression
// Output: return the next numeric value of the expression
float get_num();

// Input: none, read from stdin
// Effect: get the next operator of the expression
//         this operator can be +, -, *, /, or '\n'
//         '\n' indicates the end of the expression input
//         leading spaces should skipped
// Output: return the next operator of the expression.

char get_op();


// Input: 'sub_exp': the value of the sub s_expression to the left of 'op' location in stdin
//        'op' : an operator, '+' or '-'. 'op' could also be
//               '\n' indicating the end of the s_expression
//         the rest of the expression will be read in from stdin
// Effect: the whole s_expression is evaluated using recursion:
//         get next_num with m_exp() and then get next_op with get_op()
//         use 'sub_exp op next_num' and 'next_op' to do recursive call
// Output: this function returns the value of the s_expression

float s_exp(float sub_exp, char op);



// Input: ’sub_exp’: the value of the current sub m_expression
//                   to the left of ’op’ location in stdin.
//            ’op’ : an operator, ’*’ or ’/’. ’op’ could also be
//                   ’+’, ’-’, or ’\n’ indicating the end of the m_expression.
//                   '+’, ’-’, or ’\n’ should be pushed back to stdin.
//            the rest of the m_expression will be read in from stdin
// Effect: the m_expression is evaluated using recursion:
//         get next_num with get_num() and then get next_op with get_op()
//         use ’sub_exp op next_num’ and ’next_op’ to do recursive call
// Output: this function returns the value of the current m_expression

float m_exp(float sub_exp, char op) ;





/**
 * @fn   get_response() gets the input of the user
 * @returns the appropriate answer Y or N, or emtpy space if it is an invalid character
 * */
char get_response(){
    char tempChar = ' '; // to store the temporary character to return
    char throwAwayChar = ' '; // to store the other keys when an invalid input is entered

    scanf("%c%c", &tempChar, &throwAwayChar); // scanning for tempChar and other inputs

    // if statement to return the valid input when detected
    if ((tempChar == 'Y' || tempChar == 'N') && throwAwayChar == '\n'){
        return tempChar;
    }

    // remove the other potentially invalid characters
    while (throwAwayChar != '\n'){
        scanf("%c", &throwAwayChar);
    }
    //return empty string to signify invalid input
    return ' ';

}




char get_op() {
    char tempChar = ' '; // to hold onto the first operator which you scan for
    int escape = 0; // the condition to represent true for the while loop execution

    scanf("%c", &tempChar);

    while (escape == 0) {
        // need to eliminate for tabs
        if (tempChar == '\t' || tempChar == ' ') {
            scanf("%c", &tempChar);
        }

        // check if you got one of the operators, if so, need to return that tempChar
        if (tempChar == '*' || tempChar == '/'|| tempChar == '+'|| tempChar == '-'||  tempChar == '\n') {
            escape = 1;
        }

        // to catch any potentially invalid operators
        if (isalpha(tempChar) || tempChar == '%' || tempChar == '^' || tempChar == '#' || tempChar == '@' || tempChar == '!'
        || tempChar == '(' || tempChar == ')' || tempChar == '{' || tempChar == '}' || tempChar == '\\' || tempChar == '~'){
            printf("Invalid Character Input: %c", tempChar );
            exit(EXIT_FAILURE);
        }
    }
    return tempChar;
}


float get_num (){
    // get the float number and return it
    float tempNum  = 0;
    scanf(" %f", &tempNum);
    return tempNum;
}




float m_exp(float sub_exp, char op ){

    //check to make sure that you not dealing with addition,subtraction, or the end of the statment
    if (op == '+' || op == '-' || op == '\n'){
        // you push back the character back to standard input, that way you can call get_op()
        // without having to worry about mixing up the pattern of operator and float
        ungetc(op, stdin);
        return sub_exp;
    }
    else {
        float f1 = get_num();//need to get the number to which to do the calculation on
        char op1 = get_op(); // then the operator of the next operation

        //perform the proper operation on with the correct op
        if (op == '*'){
            f1 = sub_exp * f1;
        }
        else if (op == '/'){
            f1 = sub_exp / f1;
        }

        // return but also call this function, that way you can ensure that if the next op, op1, is either division,
        // multiplicaiton, it is performed, otherwise return what the function returns
        return m_exp(f1, op1);
    }
}


float s_exp(float sub_exp, char op){

    // condition so that if we are at the end of the line, to stop doing any get_num and get_op
    if (op == '\n'){
        return sub_exp;
    }
    else {
        float f1 = m_exp(1, '*'); // getting number by making sure that there is no * or / operation
        char op1 = get_op(); // getting the operand

        //perform proper calculation based on op
        if (op == '+'){
            f1 = sub_exp +  f1;
        }
        else if (op == '-') {
            f1 = sub_exp -  f1;
        }

        //returns the result with this f1 and the next op1
        return s_exp(f1, op1);

    }
}



int main() {

    printf("Welcome To Evaluate! A software to calculate simple arithmetic expressions that use '+'. '-', '/' or '*'\n");

    // first for loop to loop as many times need to have the integer printed in segment display as many times as needed
    for (;;) {
        printf("Please enter in a valid arithmetic expression:");
        printf("%f", s_exp(0, '+')); //
        char input = ' ';

        // this for loop checks your input reply to see if you want to enter in another integer
        for (;;) {
            printf("\nWould you like to enter another arithmetic expression?\nEnter 'Y' for yes and 'N' for no: ");
            input = get_response();

            // if statement to terminate the process if you are done with the program
            if (input == 'N') {
                printf("\nThanks for using Evaluate!\n");
                break;
            }
                // to leave this loop but still be able to enter in another value
            else if (input == 'Y'){
                break;
            }

                // to indicate to user that they have entered in an invalid input
            else if (input == ' ') {
                printf("\ninvalid input.\n");
            }
        }
        // to break from the final for loop if the person is done with the program
        if (input == 'N'){
            break;
        }

    }

    //when program is complete
    return 0;
}