#include <stdio.h>

/**
 * @author M. Hadi Badawi
 * @details the purpose of this software is to display an integer with a seven segmented display representaiton
 * */


// this 3 dimensional array stores each number's seven segment display
const char segments[10][3][3] =
        {
                { // zero representation
                        {' ', '_', ' '},
                        {'|', ' ', '|'},
                        {'|', '_', '|'}
                },

                {// one representation
                        {' ', ' ', ' '},
                        {' ', ' ', '|'},
                        {' ', ' ', '|'}
                },

                { // two rerepsenation
                        {' ', '_', ' '},
                        {' ', '_', '|'},
                        {'|', '_',' '}
                },

                { // three representation
                        {' ', '_', ' '},
                        {' ', '_', '|'},
                        {' ', '_', '|'}
                },

                { // four representation
                        {' ', ' ', ' '},
                        {'|', '_', '|'},
                        {' ', ' ', '|'}
                },

                { // five representation
                        {' ', '_', ' '},
                        {'|', '_', ' '},
                        {' ', '_','|'}
                },

                { // six representation
                        {' ', '_', ' '},
                        {'|', '_', ' '},
                        {'|', '_', '|'}
                },

                { // seven representaiton
                        {' ', '_', ' '},
                        {' ', ' ', '|'},
                        {' ', ' ', '|'}
                },

                { // eight representation
                        {' ', '_', ' '},
                        {'|', '_', '|'},
                        {'|', '_','|'}
                },

                { // ninth represenation
                        {' ', '_', ' '},
                        {'|', '_', '|'},
                        {' ', '_', '|'}
                }
        };
// this 2 dimenstional array stores the negative sign seven segment represenation
const char negative[3][3] =
        {
                {' ', ' ', ' '},
                {' ', '_', ' '},
                {' ', ' ', ' '}
        };



/**
 * @fn get_num obtains a number from input
 * @returns an an integer from the input
 * */
int get_num(){
    int tempInt = 0; // will store the integer to return
    char tempChar = ' '; // to grab the next characters until you have nothing left to avoid messing up next inputs

    scanf("%d", &tempInt); // scanf to get the first integer

    scanf ("%c", &tempChar); // get the next character

    // while loop to get ride of the rest of the input including \n
    while (tempChar != '\n'){
        scanf ("%c", &tempChar);
    }

    // the input the user entered
    return tempInt;
}

/**
 * @fn   get_response() gets the input of the user
 * @returns the appropriate answer Y or N, or emtpy space if it is an invalid character
 * */
char get_response(){
    char tempChar = ' '; // to store the temporary character to return
    char throwAwayChar = ' '; // to store the other keys when an invalid input is entered

    scanf("%c%c", &tempChar, &throwAwayChar); // scanning for tempChar and other inputs

    // if statement to return the valid input when detected
    if ((tempChar == 'Y' || tempChar == 'N') && throwAwayChar == '\n'){
        return tempChar;
    }

    // remove the other potentially invalid characters
    while (throwAwayChar != '\n'){
        scanf("%c", &throwAwayChar);
    }
    //return empty string to signify invalid input
    return ' ';

}


/**
 * @fn   print_display() displays the integer that one would like to be displayed in seven segment
 * @param  integer is the number whh hte user wants to be displayed
 * @return void, it does not return anything, just displays the integer right away
 * */
void print_display(int integer) {


    int keepTrack = 1; // will essentially store the largest number you can divide your input string
    int tempTrack = 1; // will store how many times you have to go through the first for loop, signifying the number of digits
    int tempInt = integer; // the number which we want to represent but we can modify this copy

    for (;;) {
    // if condition that checks you can still divide
     if ((integer / keepTrack) * -1 > 9 || (integer / keepTrack)  > 9) {
            keepTrack *= 10;
            tempTrack++;
        } else {
            break;
        }
    }


    int x; // will store how large the array should to display the integer


    // condition to increase the size of x which is linked to the array size if the number is positive or negative
    if (tempInt > 0) {
        x = 3 * (tempTrack );
    }

    else { x = 3 * (tempTrack + 1);
    }

    // declaring the array that will be used to display the result
    char tempArr[3][x];


    // starting index to store each segment of the each number
    int index = 0;

    // again, checks to see if the input number which we obtained is actually negative or positive
    // and will display the sign based on charge symbol for negative charge
    if (tempInt<0) {

        for (int i = 0; i<3; i++) {
            tempArr[0][i] = negative[0][i];
            tempArr[1][i] = negative[1][i];
            tempArr[2][i] = negative[2][i];
        }

        //indicating the start of the index and the change of the negative number to a positive number
        index  = 3;
        tempInt = tempInt * -1;
    }

    // this number will hold what is the modulo of tempInt
    int temp2 = 0;

    // for loop to add each number into the display array
    for(; index < x; index+=3){
        temp2 = tempInt % keepTrack; //storing the modulo
        tempInt = tempInt / keepTrack; // taking the integer division of tempInt

        // for loop to add in number repsentaiton to display array called tempArr
        for (int i = 0; i<3; i++) {
            tempArr[0][index+i] = segments[tempInt][0][i];
            tempArr[1][index+i] = segments[tempInt][1][i];
            tempArr[2][index+i] = segments[tempInt][2][i];
        }

        keepTrack /= 10; // change the base at which to divide
        tempInt = temp2; // changing this number so that the value decreases and not run into an infinite loop
    }


    // the diplay for loop to print out the seven digit segment display of the integer
    for (int i = 0; i<3;i++) {

        for (int j = 0; j < x; j++) {
            printf("%c", tempArr[i][j]);

        }

        printf("\n"); // need to add a new line after the each line is completed

    }

}

/**
 * @fn   main() is the execution body for the digital display program
 * @returns 0 when the program is complete
 *
 * */
int main() {
    printf("Welcome To Digital Display!\n");

    // first for loop to loop as many times need to have the integer printed in segment display as many times as needed
    for (;;) {
        printf("Please enter in a valid integer:");
        int integer = get_num();
        printf("\n");
        print_display(integer);
        char input = ' ';

        // this for loop checks your input reply to see if you want to enter in another integer
        for (;;) {
            printf("\nWould you like to enter another valid integer?\nEnter 'Y' for yes and 'N' for no: ");
            input = get_response();

            // if statement to terminate the process if you are done with the program
            if (input == 'N') {
                printf("\nThanks for using Digital Display!\n");
                break;
            }
            // to leave this loop but still be able to enter in another value
            else if (input == 'Y'){
                break;
            }

            // to indicate to user that they have entered in an invalid input
            else if (input == ' ') {
                printf("\ninvalid input.\n");
            }
        }
        // to break from the final for loop if the person is done with the program
        if (input == 'N'){
            break;
        }

    }


    return 0;
}