#include <stdio.h>
#include "datatype.h"
#include "stdlib.h"

/**
 * newLine is a helper funciton that will print newline when needed
 *
 */

void newLine(){
    printf("\n");
}



/**
 * @fn main() will test the matrix to see if all the methods are functioning
 * as expected
 *
 * @return 0 if program is successful, otherwise, whatever it desires
 */

int main() {

    // creating temporary veriables to use to generate keys and data
    char str1[] = "test", str2[] = "keyGenerating";
    char str3[] = "test", str4[] = "secondKeyGenerating";
    char *str5;
    int temp;

    // 2 keys will be created for testing
    Key newKey = key_gen(str1, str2);
    Key newKey2 = key_gen(str3, str4);

    // 1 data will be created for testing
    Data tempData = data_gen(5);


    printf("-------    Testing string_dup     -------\n");
    printf("Original string:%s   %p  \n", str1, &str1);

    str5 = string_dup(str1);

    printf("duplicate string: %s   %p  \n", str5, &str5);
    printf("the second parameter, memory location, should not match");

    // print newline
    newLine();


    printf("-------    Testing key_gen       -------\n");
    printf("expected result: test        keyGenerating  \n");
    printf("actual result: ");
    key_print(newKey);

    // print newline
    newLine();

    printf("expected result: test        secondKeyGenerating  \n");
    printf("actual result: ");
    key_print(newKey2);

    // print newline
    newLine();


    printf("-------    Testing key_comp      -------\n");
    printf("expected result: -1 \n");
    printf("actual result: %d\n", key_comp(newKey, newKey2));
    newLine();
    printf("expected result: 0 \n");
    printf("actual result: %d\n",key_comp(newKey, newKey));
    newLine();
    printf("expected result: 1 \n");
    printf("actual result: %d\n", key_comp(newKey2, newKey));
    newLine();



    printf("-------    Testing data_gen      -------\n");
    printf("expected result: 5 \n");
    printf("actual result: ");
    data_print(tempData);

    // print newLine
    newLine();


    printf("-------    Testing data_set      -------\n");
    printf("expected result: 7 \n");
    printf("actual result: ");
    // setting temp to a new value to test the data_set
    temp = 7;
    data_set(tempData, temp);
    data_print(tempData);

    newLine();



    printf("-------    Testing key_free      -------\n");
    printf("expected result:  not what you expect, random symbols and characters \n");
    printf("actual result: ");
    key_free(newKey);

    key_print(newKey);

    newLine();


    printf("-------    Testing key_free      -------\n");
    printf("expected result:  not what you expect, a random number \n");
    printf("actual result: ");
    data_free(tempData);

    data_print(tempData);

    newLine();

}