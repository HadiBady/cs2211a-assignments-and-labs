#include <stdio.h>
#include "datatype.h"
#include <stdlib.h>
#include <string.h>

/**
 * @fn string_dup makes a copy of the string and returns the copy of the string
 * @param str  is the string which it intakes
 * @return  returns the copy of the original string
 */

char *string_dup(char *str){
	// the string to send back
	char * string_send = (char *) malloc(sizeof(char)*strlen(str)+1);
	// the index to insert each character in the correct spot
	int i = 0;
	// only want to input items that are in the string
	while (str[i]!='\0') {
		string_send[i] = str[i];
		i++;
	}
	// put the last character as the null character to end the string
	string_send[i] = '\0';
	return string_send;
}


/**
 * key_gen creates a new key and returns that key
 * @param skey1   is the first index of the key
 * @param skey2   is the second index of the key
 * @return     the new key
 */
Key key_gen(char *skey1, char *skey2){
	//memory allocation for the new key
	Key p = (Key_struct *) malloc(sizeof(Key_struct));
	// want to duplicate both strings
	p->skey1 = string_dup(skey1);
	p->skey2 = string_dup(skey2);
	return p;
}

/**
 * key_comp takes in 2 keys, key 1 and key 2 , compares them and returns the result
 * @param key1  is the first key to compare
 * @param key2  is the second key, which the first key is compare with respect to the second
 * @return       the value o the comparison
 */
int key_comp(Key key1, Key key2){
	//first compare if the skey1's match for both keys
	if (strcmp(key1->skey1, key2->skey1) == 0){
		if (strcmp( key1->skey2, key2->skey2) < 0){
			return -1;
		}
		else if (strcmp(key1->skey2, key2->skey2) > 0){
			return 1;
		}
	}
	// this condition checks when the skeys do not match for both keys
	else {
		if (strcmp(key1->skey1, key2->skey1) < 0) {
			return -1;
		} else if (strcmp(key1->skey1, key2->skey1) > 0) {
			return 1;
		}
	}
}
/**
 * key_print prints the strings stored in key
 * @param key
 */
void key_print(Key key){
	printf("%-20s%-20s", key->skey1, key->skey2);
}


/**
 * key_free frees the key's contents then frees itself
 * @param key    is the key which it's content and itself are freed
 */
void key_free(Key key){
	free(key->skey1);
	free(key->skey2);
	free(key);
}

/**
 * data_gen generates a new data variable
 * @param idata     takes in an integer to which is stored in the new data
 * @return   		the new data generated
 */
Data data_gen(int idata){
	Data d = malloc(sizeof(Data));
	*d = idata;
	return d;
}

/**
 * data_set sets the value of data to a new value
 * @param data 		is the data item which its value will change
 * @param idata     is the value which will be inserted into data
 */
void data_set(Data data, int idata){
	*data = idata;
}

/**
 * data_print prints the value stored in data
 * @param data 		is the data item which its contents will be printed
 */
void data_print(Data data){
	printf("%9d", *data);
}
/**
 * data_free frees the contents of data
 * @param data   	is the data that will be freed
 */
void data_free(Data data){
	free(data);
}
