#include <stdio.h>
#include "bstree.h"
#include "datatype.h"
#include "matrix.h"
#include "stdlib.h"

/**
 * @fn main() will test the matrix to see if all the methods are functioning
 * as expected
 *
 * @return 0 if program is successful, otherwise, whatever it desires
 */

int main() {
    // creating a new matrix to test
    Matrix newMat = matrix_construction();

    // creating test key and data for this matrix
    char str1[] ="FirstString", str2[]="SecondString";
    char str3[] = "First", str4[]= "Second";
    char str5[] = "A", str6[] = "B";
    char str7[]= "G", str8[] = "H";

    char str9[] = "Girst", str10[] ="Gecond";
    char str11[] = "Ga", str12[] = "Ge";
    char str13[] = "Zoo", str14[] = "MadeUpWordForTest";


    printf("-------    Testing matrix_construction       -------\n");
    printf("expected result: NULL\n");
    printf("actual result: ");
    if (*newMat == NULL) {
        printf("NULL\n");
    }
    else{
        printf("Error, matrix not initialized\n");
    }

    printf("\n");
    printf("-------          Testing matrix_get          -------\n");
    printf("expected result: NULL\n");
    Value *val = matrix_get(newMat, str1, str2);
    printf("actual result: ");
    if (val == NULL) {
        printf("NULL\n");
    }
    else{
        printf("Error, matrix_get test failed\n");
    }
    printf("\n");
    printf("-------          Testing matrix_isin         -------\n");
    printf("expected result: 0\n");
    unsigned char tempChar = matrix_isin(newMat, str1, str2);
    printf("actual result: %c\n", tempChar);

    printf("\n");
    printf("-------          Testing matrix_set          -------\n");
    printf("expected result: 1\n");
    matrix_set(newMat, str1, str2, 1);
    val = matrix_get(newMat, str1, str2);
    printf("actual result: ");
    if (*val == 1) {
        printf("1\n");
    }
    else{
        printf("Error, matrix_set test failed\n");
    }

    printf("\n");
    printf("-------          Testing matrix_inc          -------\n");
    printf("expected result: 2\n");
    matrix_inc(newMat, str1, str2, 1);
    val = matrix_get(newMat, str1, str2);
    printf("actual result: ");
    if (*val == 2) {
        printf("2\n");
    }
    else{
        printf("Error, matrix_inc test failed\n");
    }

    printf("\n");
    printf("-------          Testing matrix_list         -------\n");
    //Inserting new strings into the tree
    matrix_set(newMat, str3, str4, 1);
    matrix_set(newMat, str5, str6, 1);
    matrix_set(newMat, str7, str8, 1);
    matrix_set(newMat, str9, str10, 1);
    matrix_set(newMat, str11, str12, 1);
    matrix_set(newMat, str13, str14, 1);

    printf("\nexpected result:\n");
    printf("%-20s%-20s%9d\n", str5, str6, 1);
    printf("%-20s%-20s%9d\n", str3, str4, 1);
    printf("%-20s%-20s%9d\n", str1, str2, *matrix_get(newMat, str1, str2));
    printf("%-20s%-20s%9d\n", str7, str8, 1);
    printf("%-20s%-20s%9d\n", str11, str12, 1);
    printf("%-20s%-20s%9d\n", str9, str10, 1);
    printf("%-20s%-20s%9d\n", str13, str14, 1);

    printf("\nActual result:\n");
    matrix_list(newMat);
    printf("\n");

    printf("\n");
    printf("-------       Testing matrix_destruction       -------\n");
    printf("expected result: the strings and values stored to be not like previous test or segmentation fault, either one passes the test\n");
    matrix_destruction(newMat);
    printf("actual result: ");
    matrix_list(newMat);
    printf("\n");
}
