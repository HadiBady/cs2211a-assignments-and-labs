#ifndef MATRIX_H
#define MATRIX_H


#include "bstree.h"
#include "datatype.h"

typedef BStree Matrix;
typedef char* Index;
typedef int Value;



Matrix matrix_construction(void);
 //Matrix construction using bstree_ini();

unsigned char matrix_isin(Matrix, Index, Index);
// If location (index1, index2) is defined in Matrix m, then return 
// 1. Otherwise, return 0.

Value *matrix_get(Matrix, Index, Index);
// If location (index1, index2) is defined in Matrix m, then return
// a pointer to the associated value. Otherwise, return NULL.

void matrix_set(Matrix, Index, Index, Value);
// Assign value to Matrix m at location (index1, index2). If that 
// location already has value, then overwrite.

void matrix_inc(Matrix, Index, Index, Value);
// If location (index1, index2) is defined in Matrix m, then increase 
// the associated value by value. Otherwise, report error.


void matrix_list(Matrix);
// Print indices and values in the Matrix m (with bstree traversal()).

void matrix_destruction(Matrix);
// Free allocated space (with bstree_free()). When implementing BStree
// and Matrix, you are free to use helper functions.


#endif //Matrix