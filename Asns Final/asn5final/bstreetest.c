#include <stdio.h>
#include "bstree.h"
#include "datatype.h"
#include <signal.h>

/**
 * search_tester is used to see if the key was inserted into the tree
 * by search for it in the tree
 * @param bst    the binary search tree to search through
 * @param key     the key to find
 */

void search_tester(BStree bst, Key key) {
    if (bstree_search(bst, key) != NULL) {
        printf("found key\n");
    }
    else {
     printf("key missing\n");
    }
}

/** insert_search_tester to be used to insert a node into the bstree then
 *  searches for it
 * @param bst   the binary search tree
 * @param key   the key to be inserted
 * @param data  data to be stored
 */

void insert_search_tester(BStree bst, Key key, Data data){
    bstree_insert(bst, key, data);
    search_tester(bst, key); 
}




/**
 *	the main method will create a bstree, then add 5 different nodes
 *	to the tree, and from there traverse through the tree and free it
 */

int main(void) {

    char str1[] ="test", str2[]="6";
    char str3[] = "8", str4[]= "7";
    char str5[] = "4", str6[] = "5";


    // creating a BStree
    BStree newbst = bstree_ini();


    printf("-------       Testing bstree_ini      -------\n");
    printf("expected result: NULL\n");
    printf("actual result: ");
    if (*newbst == NULL) {
        printf("NULL\n");
    }
    else{
        printf("Error, bstree not initialized\n");
    }


    // testing the bstree_insert for 5 keys and bst_search for those keys

    printf("-------       Testing bstree_insert and bstree_search     -------\n");
    printf("expected result: All keys are found\n");
    printf("actual result: \n");
    insert_search_tester(newbst, key_gen("test", "6"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "7"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "5"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "8"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "4"), data_gen(6));
    // this is to test if it will store this copy or the first one
    insert_search_tester(newbst, key_gen("test", "6"), data_gen(7));


    printf("-------       Testing bstree_traversal      -------\n");
    printf("\nexpected result:\n");
    printf("%-20s%-20s%9d\n", str1, str5, 6);
    printf("%-20s%-20s%9d\n", str1, str6, 6);
    printf("%-20s%-20s%9d\n", str1, str2, 6);
    printf("%-20s%-20s%9d\n", str1, str4, 6);
    printf("%-20s%-20s%9d\n", str1, str3, 6);


    printf("actual result: ");

    // testing the bstree_traversal
    bstree_traversal(newbst);


    printf("\n-------       Testing bstree_free      -------\n");
    printf("\nexpected result: not what you think, nodes printed with values that don't make sense or\n"
           "segmentation fault, either one your test passes\n");
    // testing the bstree_free
    bstree_free(newbst);
	bstree_traversal(newbst);
}
