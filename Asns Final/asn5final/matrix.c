#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>
#include "bstree.h"
#include "datatype.h"


/** constructs a matrix, which is a bstree
 *  returns a matrix
 */
Matrix matrix_construction(void){
	Matrix matrix = bstree_ini();
	return matrix;
}

/**
 * matrix_isin is a method that returns '1' if index1 and index2 are in
 * the matrix m, otherwise return 0
 */
unsigned char matrix_isin(Matrix m, Index index1, Index index2){
	// generate a temporary key to use to search in the matrix
	Key tempKey = key_gen(index1, index2);
	
	// if the key is already in the tree, then what is return is not null
	if (bstree_search( m,tempKey) != NULL){
	    return '1';
	}
	else {
	    return '0';
    }
}

/** 
 * matrix_get gets the value (which is data from bstree) of a key that is in it
 * and returns it, otherwise it returns null
 */
Value *matrix_get(Matrix m, Index index1, Index index2){
	// generating a temporary key to use to search in the matrix m
	Key tempKey = key_gen(index1, index2);

	// bstree_search returns the data (which is Value) if present, otherwise null
	Value *val = bstree_search(m, tempKey);

	return val;
}

/**
 * matrix_set takes in a matrix m, index1, and index2, and stores it in the matrix
 * with a value of value if it it does not exist, otherwise, it inserts it into
 * the matrix and sets the data as value
 */
void matrix_set(Matrix m, Index index1,Index index2, Value value){
	// generating key and data to be used
	Key tempKey = key_gen(index1, index2);
	Data tempData = data_gen(value);

	// bstree_search returns the data if present, otherwise returns null
	Data val = bstree_search(m, tempKey);

	// if we don't get NULL, then that means a value is already present, and we
	// just have to set it to the new value
	if (val != NULL){
		*val = value;
	}
	// this is when we get NULL, so we simply insert a new node into the bstree (Matrix)
	else{
		bstree_insert(m, tempKey, tempData);
	}

}


/**
 * matrix_inc takes in a matrix, 2 indexes, and value, to increment the value
 * if the key is already present, otherwise, report the error by print a statement
 * to the screen
 */
void matrix_inc(Matrix m, Index index1, Index index2, Value value){
    // can simply call matrix_get to return us a pointer to the value in the matrix
    Value *val = matrix_get(m, index1, index2);

    // if val is not NULL, thus the value was stored already into the matrix, then simply
    // call matrix_set with val incremented with value
    if (val != NULL){
        matrix_set(m, index1, index2, *val+=value);
    }
    // otherwise, print to the console that the key is not in the matrix
    else{
        printf("There seems to be an error which adjusting the value at indexes \"%s\" and \"%s\"", index1, index2);
    }
}

/**
 * matrix_list simply prints the contents of the matrix using in order traversal
 * for the parameter m, which takes in a matrix
 */
void matrix_list(Matrix m){
    // print indices and values in the matrix m (with bstree_traversal())
    bstree_traversal(m);
}

/**
 * matrix_destruction takes the paramter m (which is a matrix), and frees the memory allocation
 * for it
 */
void matrix_destruction(Matrix m){
	bstree_free(m);
}
