#include<stdio.h>
#include<math.h>

double piHelper(long long  n, long long currentN) {
    double temp = 0.0;
    for (; currentN <=n ; currentN++) {
        if (currentN % 2 == 1) {
            temp += (4 / (double) (2 * currentN - 1));
        } else {
            temp -= (4 / ((double) 2 * currentN - 1));
        }
    }
    return temp;
}

double piCalculation(double E){
    //calculating the value for n, in order to use recursion
    double n =(((4/E)+1)/2)-1;
    long long nNew = (int) round(n)+ 1;
    printf("%lf %lld\n", n, nNew);
    return piHelper(nNew, 1);
}

void main(){
    double tempE;
    printf("Please enter in a value for \"e\" such that the constant pi is\ncalculated.");

    scanf("%lf", &tempE);

    double pi = piCalculation(tempE);

    printf("\nThe value of pi with \"e\" of %lf is %lf\n", tempE, pi);

}


