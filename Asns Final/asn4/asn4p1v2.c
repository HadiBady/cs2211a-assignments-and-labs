/**
 *
 * I had to include some of these due to running into errors for somereason
 *
 * */
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <wchar.h>
#include <stdio.h>
#include <string.h>
#include "bst.h"


/**
 * 
 * @fn  main intakes a file or input from user input
 * it sorts the tree and prints the items in "in order" traversal
 * and frees the memory
 *
 */
int main(void) {

    BStree bst; // creating a new bst tree
    bst = bstree_ini(1000); // stating the size of the bstree
    char name[256]; // had to limit to 256, otherwise, i get segmentation fault
    int id =0, data=0; // had to initialize a value ro the ints

	// condition to make sure to only insert keys that actually have the 3
	// parameters
    while (scanf("%s%d%d", name, &id, &data)== 3){
        bstree_insert(bst, key_construct(name, id), data);
    }
   
    bstree_traversal(bst); //this traversal prints the items of tree in 
			   //"in order"
    bstree_free(bst); //freeing the memeory of the tree

}
