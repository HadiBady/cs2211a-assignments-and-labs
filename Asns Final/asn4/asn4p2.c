#include<stdio.h>
#include<math.h>


//this main method calculates approxmiately the value of pi
void main(){
    // tempE holds the epsilon value
    double tempE;
    printf("Please enter in a value for \"e\" such that the constant pi is\ncalculated:");

   scanf("%lf", &tempE);

    //calculating the value for n, in order to use for the for loop
    double n =(((4/tempE)+1)/2)-1;
    long long nNew = (long long) round(n); // recordes the rounded version of n

    double temp = 0.0; // will hole the pi value
    for (long long currentN = 1; currentN <=nNew ; currentN++) {
        if (currentN % 2 == 1) {//to catch the case where you add 
		temp += (4 / (double) (2 * currentN - 1));
        	} 
	else { //to catch the case where you subtract
           	 temp -= (4 / ((double) 2 * currentN - 1));
       	}
   }

    printf("\nThe value of pi with \"e\" of %.10lf is %.10lf\n", tempE, temp);

}


