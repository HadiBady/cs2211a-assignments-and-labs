
#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"


/**
 *	getStrs is a "helper method" to main that does the hard work to grab 2 strings and
 *	insert them into a matrix. it takes in a parameter of m (Matrix) to store those
 *	values in the matrix. If the key is already present, simply increment by 1, if the key
 *	is not present, simply insert it into the matrix
 *
 *	this method does not return any values
 *
 */

void getStrs(Matrix  m){
    // set the array of characters to each only take in a max of 256 chars, as the max for string buffer
    // is 256
    char str1[256], str2[256];

    char tempChar = '\0', tempChar2 = '\0'; // to hold onto the first operator which you scan for
    int escape = 0, escape2 = 0, inc1 = 0, inc2 = 0; // the condition to represent true for the while loop execution
    
    // scans for the first character entered
    scanf("%c", &tempChar);

    // only enter this while loop if the first character is not new line, is not NULL (meaning nothing 
    // was entered, or if the escape value is not 0, as this value is used to let us escape if there 
    // are no more things to scan
    while (escape == 0 && tempChar != '\n' && tempChar != '\0') {
        // need to eliminate for tabs and spaces
        while (tempChar == '\t' || tempChar == ' ') {
            scanf("%c", &tempChar);
        }

        // when you get there, we have eliminated the tabs and spaces, which means
        // it is time to store these characters into str1, and we only do this
        // for 1 "string" at a time, so if we run into a space or tab or new line or
        // our array needs to store \0 at the very end, so if we have too many characters
        // like 255 characters then we stop
        while (tempChar != '\n' && tempChar != ' ' && tempChar != '\t' && inc1 < 255) {
	    // storing the character into the string
            str1[inc1] = tempChar;
	    // incrementing the counter as we need to change the index
            inc1++;
	    // scanning for the next character
            scanf("%c", &tempChar);
        }
	// if you look at where we increment our counter, you will see that we increment up to
	// the number of characters, and we store up to n-1 number of characters index
	// but for a string we need our last memory locaiton to be '\0' or NULL, thus we put
	// \0 for the last place 
        str1[inc1]='\0';

	// is is a condition for when we see that you got a new line character and incrementor
	// did not change from 0. which means user did not enter in any other characters
	// from space or tab or new line so we exit out of this while loop by breaking
        if (tempChar == '\n' || inc1  == 0){
            break;
        }

	// now scanning for next string
        scanf("%c", &tempChar2);
	
	// this while loop is like the previous one execpt for the few if statments before the end
        while(escape2 == 0 && tempChar2 != '\n'&& tempChar2 != '\0'){
            while (tempChar2 == '\t' || tempChar2 == ' ') {
                scanf("%c", &tempChar2);
            }

            while(tempChar2 != '\n' && tempChar2 != ' ' && tempChar2 != '\t') {
                str2[inc2] = tempChar2;
                inc2++;
                scanf("%c", &tempChar2);
            }

            str2[inc2]='\0';


	    // if this condition is true, then we must have only 1 string entered
	    // which means we must break out of this while loop, and we must set
	    // the escape values to 1 since it means the user entered in an incorrect stirngs
	    // and we cannot insert incorrect strings or 1 string into the matrix
	    // so we break
            if (tempChar2 == '\n' && inc2 == 0){
                escape =1;
                escape2 = 1;
		break;
            }

	    // if we here, then we must break out since we got the 2 strings but
	    // there could be more strings to grab and insert into the matrix
	    // so we assign 0 to both escape values and break from only thsi while loop
	    // the not other one
            else{
                escape = 0;
                escape2 = 0;
                break;
            }
        }

	// now that we are out of the second while loop for str2 but not str1, we must insert
	// in the 2 strings into the matrix but first we search if it is in the matrix,if it is
	// in the matrix, we increment the value by 1, otherwise we set it to value of 1
        if (matrix_isin(m,  str1, str2) == '1'){
            matrix_inc(m, str1, str2, 1);
        }
        else {
            matrix_set(m, str1, str2, 1);
        }

	// this for loop is to reset the strings to NULL at each index before using it again
	// don't need to go all the way to 256 characters unless needed to by this condition
        for(int i = 0; i < inc1 && i < inc2; i++){
            str1[i] = '\0';
            str2[i] = '\0';
        }
	// resetting the increment values
        inc1 = 0;
        inc2 = 0;
	// resetting the tempChar values to NULL values
    	tempChar = '\0';
	tempChar2 = '\0';
	
	//need to scan for the next character
	scanf("%c", &tempChar);

    }
}


/**
 * the main method creates a matrix, inserts those strings and stores the number of times
 * those combinations of strings are entered, and prints you a list with the strings in order
 * and with their number of occurrences, it returns 0 or successfully run
 */

int main(void) {
    // making the new matrix to store the strings in it
    Matrix newMat =  matrix_construction();

    // call the helper method to input the strings into the matrix
    getStrs(newMat);


    // strings stored to be used to print the strings and occurences in an organized fashion
    char *st1 = "String 1", *st2 = "String 2", *st3 = "Occurrence";
    
    // printing st1, st2, and st3 in a formated fashion
    printf("%-20s%-20s%-8s", st1, st2, st3); 

    // now printing the matrix by calling matrix_list
    matrix_list(newMat);

    // printing the new line after the end of the matrix
    printf("\n");

    // destroying the matrix before the end of the program
    matrix_destruction(newMat);
}
