#include "bstree.h"
#include <stdio.h>
#include <stdlib.h>
#include "datatype.h"


/**
 *	bstree_ini initializes the bstree and return that bstree
 */

BStree bstree_ini(void){
	BStree bst; // this will be the bstree that will be returned
	// memory allocating for bstree, which is also bstree_node **
	// because i am using BStree_node **, then i have to cast to BStree
	bst = (BStree) malloc(sizeof(BStree_node **));
	// the bstree needs to be NULL to add items to it later
	*bst = NULL; 
	return bst;
}

/**
 * bstree_insert_helper takes in a node, a key, and data
 * point of this function is to insert the new node recussively
 * function does not return anything
 * this is a helper method for bstree_insert
 */
void bstree_insert_helper(BStree_node *currentNode, Key key, Data data) {
	// if Key is greater than currentNode's key then we enter this condition
	if (key_comp(key, currentNode->key) > 0) {
		// if the right child exists, then we need to call this function
		// on the right child, otherwise, insert a new node at this location
		if (currentNode->right != NULL) {
			bstree_insert_helper((BStree_node *) currentNode->right, key, data);
		}
		else{
			// inserting the new node at this location
			currentNode->right=(struct BStree_node *)new_node(key,data);
		}
	}
	// if Key is less than the currentNode, then we enter this condition
	else if (key_comp(key, currentNode->key) < 0) {
		// of tje left child exits, then we need to call this function
		// on the left child, otherwise, insert a new node at this location
		if (currentNode->left != NULL) {
			bstree_insert_helper((BStree_node *) currentNode->left, key, data);
		}
		else{
			// inserting the new node at this location
			currentNode->left = (struct BStree_node *)new_node(key,data);
		}
	}
	// regarding the case where the user is trying to insert the same key, we simply
	// do not perform any functions regarding that case
}

/**
 *

BStree_node *bstree_helper(BStree_node *currentNode, Key key) {
	if (currentNode == NULL){
		return currentNode;
	}
	else{
		if (key_comp(key, currentNode->key) > 0) {
			if (currentNode->right !=NULL){
				return bstree_helper((BStree_node *)currentNode->right, key);
			}
			else {
				return (*currentNode).right;
			}
		}
		else if (key_comp( key, currentNode->key) < 0) {
			if (currentNode->left != NULL){

				return bstree_helper((BStree_node *)currentNode->left, key);
			}
			else{
				return (*currentNode).left;
			}
		}
	}
}
*/

/**
 * bstree_search_helper takes in 2 paramters, a BStree_node and a Key, to find if the item
 * is in the bstree and return the data, otherwise returning NULL;
 * this is a helper method for bstree_search
 */

Data bstree_search_helper(BStree_node *currentNode, Key key) {
	// condition to return NULL if the currentNode is empty
	if (currentNode == NULL){
		return NULL;
	}
	// otherwise, we are search for the function
	else{
		// if there is a match of the key, then return data stored at that node
		if (key_comp(currentNode->key, key) == 0 ){
			return currentNode->data;
		}
		// if the key is greater than the currentNode, then return the call of the function
		// on the right child
		else if (key_comp(key, currentNode->key) > 0) {
			return bstree_search_helper((BStree_node *)currentNode->right, key);
		}
		// if the key is less than the currentNode, then return the call of the function
		// on the left child
		else if (key_comp( key, currentNode->key) < 0) {
			return bstree_search_helper((BStree_node *)currentNode->left, key);
		}
	}
}

/**
 *	bstree_insert takes in 3 parameters, bst, key, and data which insert a new node
 *	if it is not in the tree, otherwise, it ignores it
 */

void bstree_insert(BStree bst, Key key, Data data){
	// inserting the key into the bstree if it is empty
	if (*bst == NULL){
		*bst = new_node(key_gen(key->skey1, key->skey2),data_gen(*data));
	}
	// otherwise, let bstree_insert_helper perform the insertion
	else{
		bstree_insert_helper((*bst), key, data);

	}
}

/**
 *	new_node takes in 2 parameters, key and data. It performs
 *	the operations needed to create a new node, generate a new key,
 *	generate a new data, sets both children to null, and returns 
 *	the node as a pointer
 */
BStree_node * new_node(Key key, Data data) {
	// memory allocation for BStree_node
	BStree_node *newNode = (BStree_node *) malloc(sizeof(BStree_node));
	// generating the newNode's key by taking the 2 strings from key 
	newNode->key = key_gen(key->skey1, key->skey2);
	// generating the newNode's data by taking the data from data
	(newNode->data) = data_gen(*data);
	// setting left and right children of newNode to NULL
	(*newNode).left = NULL;
	(*newNode).right = NULL;
	return newNode; // returning newNode, the function will then return the pointer of this
}

/**
 * bstree_search takes in 2 parameters, bst and key, to search in the bstree
 * to see if the key is present and then it return the data stored. otherwise,
 * it returns NULL
 */
Data bstree_search(BStree bst, Key key){
	// if the bstree is empty, we return null
	if (*bst == NULL){
		return NULL;
	}
	
	// if the bstree's root is the key, we return the key's data
	else if (key_comp((*bst)->key, key) == 0 ){
		return (*bst)->data;
	}
	
	// if the key is greater than bst's root key, then call bstree_search_helper
	// with the right child as the currentNode, and key as key
	else if(key_comp((*bst)->key, key) > 0) {
		return	bstree_search_helper((BStree_node *)(*bst)->left, key);
	}

	// if the key is less than bst's root key, then call bstree_search_helper
	// with the left child as the currentNode, and key as key
	else if (key_comp((*bst)->key, key) < 0) {
		return  bstree_search_helper((BStree_node *)(*bst)->right, key);
	}
}

/**
 * 	bstree_traversal_helper takes in 2 parameters, bst and currentNode
 * 	it will recussively call itself to print the contents of currentNode's left child,
 * 	then currentNode, then currentNode's right child, if they exist, otherwise it prints
 * 	nothing.
 * 	it is a helper method for bstree_traversal
 */
void bstree_traversal_helper(BStree bst, BStree_node *currentNode){
	// if the left child of currentNode is not NULL, we call bstree_traversal_helper
	// with currentNode's left child as a parameter
	if ((currentNode->left) != NULL){
		bstree_traversal_helper(bst, (BStree_node *)currentNode->left);
	}
	// if the currentNode exists, then we print the key and data of currentNode
	if ( currentNode != NULL && (currentNode->key) != NULL && (currentNode)->data != NULL){
		printf("\n");
		key_print((currentNode->key));
		printf(" ");
		data_print((*currentNode).data);
	}
	
	// if the currentNode has a left child, then we call bstree_traversal_helper
	// with currentNode's right child as a parameter
	if (currentNode->right != NULL){
		bstree_traversal_helper( bst, (BStree_node *)currentNode->right);
	}

}

/**
 *	bstree_traversal takes in parameter, bst, which calls bstree_traversal_helper
 *	to traverse if the bstree contains any nodes
 */
void bstree_traversal(BStree bst) {
	if ((*bst) != NULL) {
		bstree_traversal_helper(bst, &**bst);
	}
}

/**
 *	bstree_free_helper is a helper method for bstree_free which recurssively
 *	frees the current node's memory and memory allocated for that node, and 
 */
void bstree_free_helper(BStree_node *bt) {
	// if the left child of that node is not null, then call the free function on it
	if ((*bt).left != NULL) {
		bstree_free_helper((BStree_node *)(*bt).left);
	}
	// if the right child of that node is not null, then call the free function on it
	if ((*bt).right != NULL) {
		bstree_free_helper((BStree_node *)(*bt).right);
	}
	// if the key component has a key stored there, call the key_free
	if ((*bt).key != NULL) {
		key_free((*bt).key);
	}
	// if the data component has a data stored in it, then call the data_free on it
	if ((*bt).data != NULL) {
		data_free((*bt).data);
	}
	// call free on the node to free the memory allocation for the node
	free(bt);

}

/**
 *	bstree_free frees the memory allociated for a bstree, including the nodes
 *	and the components of the nodes
 *	uses the helper method bstree_free_helper
 */

void bstree_free(BStree bst){
	// if the binary search tree contains values, we free it
	if (*bst != NULL) {
		bstree_free_helper(&(**bst));
	}
	// free the memory allocated for a bstree
	free(bst);
}
