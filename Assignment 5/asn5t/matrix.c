#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>
#include "bstree.h"
#include "datatype.h"

Matrix matrix_construction(void){
	Matrix matrix = malloc(sizeof(BStree));
	return matrix;
}

unsigned char matrix_isin(Matrix m, Index index1, Index index2){
	// if (index1, index2) os im matrix m, then return 1
	// otherwise return 0;
	Key tempKey = key_gen(index1, index2);

	if (bstree_search( m,tempKey) != NULL){
	    return '1';
	}
	else {
        return '0';
    }
}

Value *matrix_get(Matrix m, Index index1, Index index2){
	// if (index1, index2) is in matrix m, then return pointer value
	// otherwise return NUll

	Key tempKey = key_gen(index1, index2);

	Value *val = bstree_search(m, tempKey);

	return val;
}

void matrix_set(Matrix m, Index index1,Index index2, Value value){
	// assign value to Matrix m at location (index1, index2)
	// if there is a value there, overwrite it

	Key tempKey = key_gen(index1, index2);
	Data tempData = data_gen(value);

	Data val = bstree_search(m, tempKey);

	if (val != NULL){
		*val = value;
	}
	else{
		bstree_insert(m, tempKey, tempData);
	}

}

void matrix_inc(Matrix m, Index index1, Index index2, Value value){
	// if location (index1, index2) is defined in matrix m then increase
	// the value by value, otherwise report error

    Value *val = matrix_get(m, index1, index2);

    if (val != NULL){
        matrix_set(m, index1, index2, *val+=value);
    }
    else{
        printf("There seems to be an error which adjusting that value at indexes \"%s\" and \"%s\"", index1, index2);
    }
}

void matrix_list(Matrix m){
	// print indices and values in the matrix m (with bstree_traversal())
    bstree_traversal(m);
}


void matrix_destruction(Matrix m){
	bstree_free(m);
}
