#include<stdio.h>
#include "datatype.h"


/**
 * newLine is a helper funciton that will print newline when needed
 *
 */

void newLine(){
	printf("\n");
}



/**
 *	this main method will test, generating a key, generating data, and 
 *	print the keys and data, seting data to different value and freeing
 *	both keys and data
 */

void printHelper(){
	printf("\n");
}

int main(void){

	// creating temporary veriables to use to generate keys and data
	char *str1 , *str2 ;
	int temp;

	// 2 keys will be created for testing
	Key newKey = NULL; 
	Key newKey2 = NULL;

	// 1 data will be created for testing
	Data tempData = NULL;

	// seting str1 and str2 to strings to generate newKey
	
	printf("newKey:");
	str1 = "test";
	str2 = "key generating";
        newKey = key_gen(str1, str2);

	// printing out newKey
	key_print(newKey);	

	// print newline
	newLine();

	
	printf("tempData:");

	// setting value for temp to use to generate data
	temp = 5;
	tempData = data_gen(temp);

	// printing data
	data_print(tempData);
	
	// print newLine
	newLine();

	
	printf("tempData updated:");

	// setting temp to a new value to test the data_set
	temp = 7;
	data_set(tempData, temp);

	data_print(tempData);

	newLine();

	printf("newKey2:");
	
	// str1 and str2 are updated to create newKey2
	str1 = "test";
	str2 = "second key generating";
	newKey2 = key_gen(str1, str2);

	// rpintint out newKew2
	key_print(newKey2);

	newLine();

	// testing out the free method
	key_free(newKey);
	
	key_print(newKey);	
	
	newLine();

	printf("if you don't see newKey printed or see something else \nthen successfully freed newKey\n");



	// testing out the free method
	key_free(newKey2);

	key_print(newKey);	

	newLine();
	
	printf("if you don't see newKey2 printed or see something else \nthen successfully freed newKey2\n");



	data_free(tempData);
	
	data_print(tempData);	

	newLine();

	printf("if you don't see tempData printed or see something else \nthen successfully freed tempData\n");



	// this is when we reach the end of the test
	printf("if you reach this part, then the test has passed.\n");

}
