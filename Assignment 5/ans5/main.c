
#include <stdlib.h>
#include <stdio.h>
#include "matrix.h"

void getStrs(Matrix  m){
    char str1[256], str2[256];

    char tempChar = ' ', tempChar2 = ' '; // to hold onto the first operator which you scan for
    int escape = 0, escape2 = 0, inc1 = 0, inc2 = 0; // the condition to represent true for the while loop execution

    scanf("%c", &tempChar);

    while (escape == 0 && tempChar != '\n') {
        // need to eliminate for tabs
        while (tempChar == '\t' || tempChar == ' ') {
            scanf("%c", &tempChar);
        }

        // check if you got one of the operators, if so, need to return that tempChar
        while (tempChar != '\n' && tempChar != ' ') {
            str1[inc1] = tempChar;
            inc1++;
            scanf("%c", &tempChar);
        }
        str1[inc1]='\0';

        if (tempChar == '\n' || inc1  == 0){
            break;
        }

        scanf("%c", &tempChar2);

        while(escape2 == 0 && tempChar2 != '\n'){
            while (tempChar2 == '\t' || tempChar2 == ' ') {
                scanf("%c", &tempChar2);
            }

            // check if you got one of the operators, if so, need to return that tempChar
            while(tempChar2 != '\n' && tempChar2 != ' ') {
                str2[inc2] = tempChar2;
                inc2++;
                scanf("%c", &tempChar2);
            }

            str2[inc2]='\0';

            if (tempChar2 == '\n' && inc2 == 0){
                escape =1;
                escape2 = 1;
                break;
            }
            else{
                escape = 0;
                escape2 = 0;
                break;
            }
        }

        if (matrix_isin(m,  str1, str2) == '1'){
            matrix_inc(m, str1, str2, 1);
        }
        else {
            matrix_set(m, str1, str2, 1);
        }

        for(int i = 0; i <256; i++){
            str1[i] = '\0';
            str2[i] = '\0';
        }
        inc1 = 0;
        inc2 = 0;
    }
}


int main(void) {

    Matrix newMat =  matrix_construction();


    getStrs(newMat);



    char *st1 = "String 1", *st2 = "String 2", *st3 = "Occurrence";
    printf("%-20s%-20s%-8s", st1, st2, st3);
    matrix_list(newMat);
    matrix_destruction(newMat);

}