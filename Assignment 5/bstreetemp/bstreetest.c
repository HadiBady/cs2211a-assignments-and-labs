#include <stdio.h>
#include "bstree.h"
#include "datatype.h"



int main(void) {
    BStree newbst = bstree_ini();

    bstree_insert(newbst, key_gen("test", "6"), data_gen(6));
    bstree_insert(newbst, key_gen("test", "7"), data_gen(6));
    bstree_insert(newbst, key_gen("test", "5"), data_gen(6));
    bstree_insert(newbst, key_gen("test", "8"), data_gen(6));
    bstree_insert(newbst, key_gen("test", "4"), data_gen(6));


    bstree_traversal(newbst);

    bstree_free(newbst);

    printf("Hello, World!\n");

}