#include <stdio.h>
#include "datatype.h"
#include <stdlib.h>
#include <string.h>

char *string_dup(char *str){
	char * string_send = (char *) malloc(sizeof(char)*strlen(str)+1);
	int i =0;
	while (str[i]!='\0') {
		string_send[i] = str[i];
		i++;
	}

	string_send[i] = '\0';
	return string_send;
}

Key key_gen(char *skey1, char *skey2){
	Key p = (Key_struct *) malloc(sizeof(Key_struct));
	p->skey1 = string_dup(skey1);
	p->skey2 = string_dup(skey2);
	return p;
}


int key_comp(Key key1, Key key2){
	if (strcmp(key1->skey1, key2->skey1) == 0){
		if (strcmp( key1->skey2, key2->skey2) < 0){
			return -1;
		}
		else if (strcmp(key1->skey2, key2->skey2) > 0){
			return 1;
		}
	}
	else {
		if (strcmp(key1->skey1, key2->skey1) < 0) {
			return -1;
		} else if (strcmp(key1->skey1, key2->skey1) > 0) {
			return 1;
		}
	}
}

void key_print(Key key){
	printf("%-20s%-20s", key->skey1, key->skey2);
}

void key_free(Key key){
	free(key->skey1);
	free(key->skey2);
	free(key);
}


Data data_gen(int idata){
	Data d = (Data) malloc(sizeof(int));
	*d = idata;
	return d;
}

void data_set(Data data, int idata){
	*data = idata;
}

void data_print(Data data){
	printf("%8d", *data);
}

void data_free(Data data){
	free(data);
}
