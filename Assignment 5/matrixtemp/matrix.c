#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>
#include "../bstreetemp/bstree.h"
#include "../datatypetemp/datatype.h"

Matrix matrix_construct(void){
	Matrix matrix = malloc(sizeof(BStree));
	return matrix;
}

unsigned char matrix_isin(Matrix m, Index index1, Index index2){
	// if (index1, index2) os im matrix m, then return 1
	// otherwise return 0;
	if (bstree_search( m,key_gen(index1, index2)) != NULL){
	    return '1';
	}
	else {
        return '0';
    }
}

Value *matrix_get(Matrix m, Index index1, Index index2){
	// if (index1, index2) is in matrix m, then return pointer value
	// otherwise return NUll

	Value *val = bstree_search(m, key_gen(index1, index2));

	return val;
}

void matrix_set(Matrix m, Index index1,Index index2, Value value){
	// assign value to Matrix m at location (index1, index2)
	// if there is a value there, overwrite it

	Value *val = bstree_search(m, key_gen(index1, index2));

	if (val != NULL){
        *val = value;
	}

}

void matrix_inc(Matrix m, Index index1, Index index2, Value value){
	// if location (index1, index2) is defined in matrix m then increase
	// the value by value, otherwise report error

    Value *val = matrix_get(m, index1, index2);

    if (val != NULL){
        matrix_set(m, index1, index2, *val+=value);
    }
    else{
        printf("There seems to be an error which adjusting that value at indexes \"%s\" and \"%s\"", index1, index2);
    }
}

void matrix_list(Matrix m){
	// print indices and values in the matrix m (with bstree_traversal())
	//
    bstree_traversal(m);
}


void matrix_destruction(Matrix m){
	bstree_free(m);
}
