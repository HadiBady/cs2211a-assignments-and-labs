#include <stdio.h>
#include "bstree.h"
#include "datatype.h"
#include <signal.h>


void search_tester(BStree bst, Key key){
 if (bstree_search(bst, key) != NULL){
	printf("found key\n");
    }
    else{
	printf("key missing\n");
    }
}

void insert_search_tester(BStree bst, Key key, Data data){
    bstree_insert(bst, key, data);
    search_tester(bst, key); 
}




/**
 *	the main method will create a bstree, then add 5 different nodes
 *	to the tree, and from there traverse through the tree and free it
 */

int main(void) {

    // creating a BStree
    BStree newbst = bstree_ini();

    // testing the bstree_insert for 5 keys and bst_search for those keys
     
    insert_search_tester(newbst, key_gen("test", "6"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "7"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "5"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "8"), data_gen(6));
    insert_search_tester(newbst, key_gen("test", "4"), data_gen(6));
    // this is to test if it will store this copy or the first one
    insert_search_tester(newbst, key_gen("test", "6"), data_gen(7));

    // testing the bstree_traversal
    bstree_traversal(newbst);


    // testing the bstree_free
    bstree_free(newbst);

    
    printf("\nif you get segementation fault error after this message then that means you have successfully \nfreed the bst.");    

 
	bstree_traversal(newbst);
   
 

}
