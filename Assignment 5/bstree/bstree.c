

BStree bstree_ini(void){
	BStree bst;
	bst = (BStree) malloc(sizeof(BStree_node **);
	*bst = null;
	return bst;

/*
	BStree object = (BStree_node *) malloc(sizeof(BStree_node));
	object->Key = NULL;
	object->Data = NULL;
	object->BStree_node->left = NULL;
	object->BStree_node->right = NULL;
	return object;
*/
}

void bstree_insert(BStree bst, Key key, Data data){
	if (*bst = NULL){
		*bst = new_Node(key, data);
	}
	else{
		if (key_cmp(*(bst->key), key) > 0){
			bstree_insert_helper(bst->right, key, data);
		}
		else if (key_cmp(*(bst->key), key) < 0){
			bst_insert_helper(bst->left, key, data);
		}
	}
}

BStree_node* bstree_helper(BStree_node *currentNode, Key key, Data data) {
	if (*currentNode = NULL){
		return currentNode;
	}
	else{
		if (key_cmp(*(currentNode->key), key) > 0) {
			bstree_insert_helper(currentNode->right, key, data);
		}
		else if (key_cmp(*(currentNode->key), key) < 0) {
			bstree_insert_helper(currentnode->left, key, data);
		}
	}
}

BStree_node *new_Node(Key key, Data data){
	BStree_node **newNode = malloc(sizeof(BStree_node));
	newNode->key = key_gen(key->skey1, key->skey2);
	newNode->data = data_gen(data);
	return newNode; 
}

Data bstree_search(BStree bst, Key key){
	if (bst == null){
		printf("The Binary Search Tree is empty.")
		return NULL;
	}
	if (key_cmp(*(bst->key), key) == 0 ){
		return *(bst->data);
	}

	else if(key_cmp(*(bst->key), key) > 0) {
		bstree_helper(bst->right, key, data);
	}
	else if (key_cmp(*(bst->key), key) < 0) {
		bstree_helper(bst->left, key, data);
	}
}

void bstree_traversal_helper(BStree_node *currentNode){
	if (*(currentNode->left) != NULL){
		bstree_traversal_helper(bst->left);
	}
	if ( *(currentNode->key) != NULL && *(bst->data) != NULL){
		printf("\n");
		key_print(*(currentNode->key));
		printf(" ");
		data_print((*currentNode).data);
	}
	if (*(currentNode->right) != NULL){
		bstree_traversal_helper(bst->right);
	}	

}

void bstree_traversal(BStree bst){
 	if (*(bst->left) != NULL){
		bstree_traversal_helper(bst->left);
	}
	if ( *(bst->key) != NULL && *(bst->data) != NULL){
		printf("\n");
}

static void bstree_free(BStree_node *bt){
	if (bt == NULL) return;
	btree_free(bt->left);
	btree-free(bt->right);
	key_free(bt->key);
	data-free(bt->data);
	free(bt);
}

void bstree_free(BStree bst){
	btree_free(*bst);
	free(bst);
}
