//
// Created by HADI BADAWI on 2018-10-07.
//


#include <stdio.h>



int scanInDig(){
    char randomChar;
    int anyInput;
    scanf("%d%c", &anyInput, &randomChar);

    return anyInput;
}

float scanInFloat(){
    char randomChar;
    float anyInput;
    scanf("%f%c", &anyInput, &randomChar);

    return anyInput;
}


float helperExpoRecur(float base, int exponent) {

    if (exponent == 1) {
        return base;
    } else if (exponent == 0) {
        return 1.0;
    } else {
        return helperExpoRecur(base, exponent / 2) * helperExpoRecur(base, exponent / 2);
    }
}


float ExpoRecurFunc(float base, int exponent){
    if ((exponent  %  2) == 0){
        return helperExpoRecur(base, exponent);
    }
    else {
        return (helperExpoRecur(base, (exponent-1)) * helperExpoRecur(base, 1));
    }
}




int main(void) {
    float inBase;
    int inExpo;

    printf("Welcome to ExpCalculator.\n");


    printf("Please enter a positive base:");
    inBase = scanInFloat();

    if (inBase < 0 || inBase == 0){
        printf("\nThat base value is invalid. Please enter a positive base:");
        inBase = scanInFloat();
    }


    printf("\nPlease enter a positive exponent:");
    inExpo = scanInDig();

    printf("\nThe value for an exponential function which has a base of %f and an exponent of %d is %f.", inBase, inExpo, ExpoRecurFunc(inBase, inExpo));

}

