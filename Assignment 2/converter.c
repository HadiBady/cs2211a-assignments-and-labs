

#include <stdio.h>

int inDig;
float inFloat;
int option;
char randomChar;


int scanInDig(){
    int anyInput;
    scanf("%d%c", &anyInput, &randomChar);

    return anyInput;
}


float scanInFloat(){
    float anyInput;
    scanf("%f%c", &anyInput, &randomChar);

    return anyInput;
}


char scanInChar(){
    char anyInput;
    scanf("%c%c", &anyInput, &randomChar);

    return anyInput;
}

void kiloMile () {
    float kiloMileCon;
    const float KiloMile = 0.62;

    for (;;) {
        printf("Please select one of the following options:\n");
        printf("Type \"K\" for conversion from Kilometer and Mile\n");
        printf("Type \"M\" for conversion from Mile to Kilometer\n");

        option = scanInChar();

        switch (option) {

            case 'K' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                kiloMileCon = inFloat * KiloMile;

                printf("%f Kilometer(s) is %.2f Mile(s)\n", inFloat, kiloMileCon);

                break;

            case 'M' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                kiloMileCon =  inFloat / KiloMile;

                printf("%f Mile(s) is %.2f Kilometer(s)\n", inFloat, kiloMileCon);

                break;

            default:
                printf("I am sorry, please selection one of the following options.\n");
        }
        break;
    }

}

void meterFeet () {

    float MeterFeetCon;
    const float MeterFeet = 3.28;

    for (;;) {
        printf("Please select one of the following options:\n");
        printf("Type \"M\" for conversion from Meter to Feet\n");
        printf("Type \"F\" for conversion from Feet to Meter\n");

        option = scanInChar();

        switch (option) {

            case 'M' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                MeterFeetCon =  inFloat * MeterFeet;

                printf("%f Meter(s) is %.2f Foot/Feet\n", inFloat, MeterFeetCon);

                break;

            case 'F':
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                MeterFeetCon = inFloat / MeterFeet;

                printf("%f Foot/Feet is %.2f Meter(s)\n", inFloat, MeterFeetCon);

                break;

            default:
                printf("I am sorry, please selection one of the following options.\n");
        }
        break;
    }

}

void centInch () {

    float CentInchCon;
    const float CentInch = 0.39;

    for (;;) {
        printf("Please select one of the following options:\n");
        printf("Type \"C\" for conversion from Centimetre to Inch\n");
        printf("Type \"I\" for conversion from Inch to Centimetre\n");


        option = scanInChar();

        switch (option) {

            case 'C' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                CentInchCon = inFloat * CentInch;

                printf("%f Centimetre(s) is %.2f Inch(s)\n", inFloat, CentInchCon);

                break;

            case 'I' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                CentInchCon = inFloat / CentInch;

                printf("%f Inch(s) is %.2f Centimetre(s)\n", inFloat, CentInchCon);

                break;

            default:
                printf("I am sorry, please selection one of the following options.\n");
        }
        break;
    }
}


void celFah () {

    float celFahCon;

    for (;;) {
        printf("Please select one of the following options:\n");
        printf("Type \"C\" for conversion from Celsius to Fahrenheit\n");
        printf("Type \"F\" for conversion from Fahrenheit to Celsius\n");


        option = scanInChar();

        switch (option) {

            case 'C' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                celFahCon = (inFloat - 32) * 5 / 9;

                printf("%f Celsius is %f Fahrenheit\n", inFloat, celFahCon);

                break;

            case 'F' :
                printf("Please enter the value you would like to convert:\n");

                inFloat = (scanInFloat());

                celFahCon = (inFloat * 9 / 5) + 32;

                printf("%f Fahrenheit is %f Celsius\n", inFloat, celFahCon);

                break;

            default:
                printf("I am sorry, please selection one of the following options.\n");
        }
        break;
    }

}


void main() {


    printf("Welcome to Converter.\n");

    for (;;){
        printf("Please select one of the following options:\n");
        printf("Type \"1\" for conversion between Kilometers and Mile\n");
        printf("Type \"2\" for conversion between Meter and Feet\n");
        printf("Type \"3\" for conversion between Centimetre and Inch\n");
        printf("Type \"4\" for conversion between Celsius and Fahrenheit\n");
        printf("Type \"5\" to quit\n");

        option = scanInChar();

        switch (option){

            case '1' :
                kiloMile();
                break;
            case '2' :
                meterFeet();
                break;
            case '3' :
                centInch();
                break;
            case '4' :
                celFah();
                break;
            case '5' :
                printf("Thanks for using Converter!");
                break;
            default:
                printf("I am sorry, please selection one fot he following options.\n");
        }
        if (option == 5){
            break;
        }

    }
}


