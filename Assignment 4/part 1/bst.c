//
// Created by hadi on 11/5/2018.
//

// ====== this is in bst.c
#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

unsigned  char *is_free;
Node *tree_nodes;
int *size ;

// Input: ’size’: size of an array
// Output: a pointer of type BStree,
// i.e. a pointer to an allocated memory of BStree_struct type
// Effect: dynamically allocate memory of type BStree_struct
// allocate memory for a Node array of size+1 for member tree_nodes
// allocate memory for an unsigned char array of size+1 for member is_free
// set all entries of is_free to 1
// set member size to ’size’;

BStree bstree_ini(int size) {
    BStree object = (BStree_struct *) malloc(sizeof(BStree_struct));
    object->tree_nodes = (Node *) malloc((size+1)* sizeof(Node));
    object->is_free = (unsigned  char *) malloc((size+1)*sizeof(unsigned char));
    for (int x = 1; x <= size; x++){
        object->is_free[x] = '1';
    }
    object->is_free[0] = '0';// we cannot insert any values in index 0
    object->size = size;
    return object;
}

void bst_insert_helper(BStree bst, Key *key, int data, int index){
    if (index > (bst->size)){
        //do something for array being overfilled
        printf("");
    }
    else {
        if (bst->is_free[index] == '1'){
            bst->tree_nodes[index].key = key;
            bst->tree_nodes[index].data = data;
            bst->is_free[index] = '0';
        }
        else {
            if (key_comp(*(bst->tree_nodes[index].key),*(key)) < 0) {
                bst_insert_helper(bst, key, data, 2*index + 1);
            } else if ( key_comp(*(bst->tree_nodes[index].key), *(key)) > 0) {
                bst_insert_helper(bst, key, data, 2*index);
            }
        }
    }
}


// Input: ’bst’: a binary search tree
// ’key’: a pointer to Key
// ’data’: an integer
// Effect: ’data’ with ’key’ is inserted into ’bst’
// if ’key’ is already in ’bst’, do nothing
void bstree_insert(BStree bst, Key *key, int data) {
    //Node newNode = {key, data};

    bst_insert_helper(bst, key, data, 1);
    /*
    if (bst->is_free[1] == '1'){
        bst->tree_nodes[1] = newNode;
        bst->is_free[1] = '0';
    }
    else {
        if (key_comp(*key, *(bst->tree_nodes[1].key)) > 0) {
            bst_insert_helper(bst, &newNode, 2 + 1);
        }
        else if (key_comp(*key, *(bst->tree_nodes[1].key)) < 0) {
            bst_insert_helper(bst, &newNode, 2);
        }
    }*/
    /*
    if (bst.size == 0){bst->tree_nodes[1] = newNode;}
    else if (bst.size > 0 && bst.size <=) {
    }*/
}



void bst_traversal_helper(BStree bst, int index){// int goForward){
   /* if (bst->is_free[index] == '1'){// && goForward == 1){
       // bst_traversal_helper(bst, index/2, 0);

    }
    else if (bst->is_free[index] == '0' && goForward == 0){
        print_node(bst->tree_nodes[index]);
    }

    else */if (bst->is_free[index] == '0'){
        bst_traversal_helper(bst, 2*index);
        //bst_traversal_helper(bst, index);
        print_node(bst->tree_nodes[index]);
        bst_traversal_helper(bst, 2*index + 1);

    }
}


// Input: ’bst’: a binary search tree
// Effect: print all the nodes in bst using in order traversal
void bstree_traversal(BStree bst) {
    int index = 1;
    //int goForward = 1;
    bst_traversal_helper(bst, index); //goForward);
}
// Input: ’bst’: a binary search tree
// Effect: all memory used by bst are freed
void bstree_free(BStree bst) {
    free(bst);//free array of nodes, then free arry of is_free, then this free
}
/*
void print_tree(BStree bst) {
    printf("\n");
    for (int x = 1; x <= bst->size + 1; x++) {
        if (bst->is_free[x] == '0') {
            print_node(bst->tree_nodes[x]);
        }
    }


}*/

