//
// Created by hadi on 11/5/2018.
//

// ====== this is in bst.c
#include <stdio.h>
#include <stdlib.h>
#include "bst.h"

//These are the 3 members of the binary search tree
unsigned  char *is_free; // this stores whether the array index is free or not
Node *tree_nodes; // this stores the actual pointers to the tree nodes
int *size ; // this stores the size of the binary search tree

// Input: ’size’: size of an array
// Output: a pointer of type BStree,
// i.e. a pointer to an allocated memory of BStree_struct type
// Effect: dynamically allocate memory of type BStree_struct
// allocate memory for a Node array of size+1 for member tree_nodes
// allocate memory for an unsigned char array of size+1 for member is_free
// set all entries of is_free to 1
// set member size to ’size’;

BStree bstree_ini(int size) {
    // "object" is the binary search tree, simply thought this was directing
    // us to think about objects
    BStree object = (BStree_struct *) malloc(sizeof(BStree_struct));
    object->tree_nodes = (Node *) malloc((size+1)* sizeof(Node));
    object->is_free = (unsigned  char *) malloc((size+1)*sizeof(unsigned char));
    for (int x = 0; x <= size; x++){
        object->is_free[x] = '1';
    }
    object->is_free[0] = '0';// we cannot insert any values in index 0, to ensure that no values are inserted here
    object->size = size;
    return object; //returning an "object"
}


/**
 *  @fn bst_insert_helper
 *  takes in the same parameters as the bst_insert except that it
 *  also does the recursion thus it also keeps track of the index
 *  inserts the key if possible, if tree cannot  insert item, it 
 *  prints a statement stating what it can't print
 *  otherwise, it inserts it unless it already exists
 */
void bst_insert_helper(BStree bst, Key *key, int data, int index){
    // to make sure that we don't insert an item outside the array
    if (index > (bst->size)){
        //prints when the array is overfilled
        printf("Sorry, the item ");
	printf("(%s %d) %d ",(*key).name, (*key).id, data);
	printf("cannot be inserted due to the size of the tree\n");
	return;
    }
    else {
	// condition to make sure that  we only insert the key if th position
	// is free and does not have an item in the array
        if (bst->is_free[index] == '1'){
            bst->tree_nodes[index].key = key;
            bst->tree_nodes[index].data = data;
            bst->is_free[index] = '0';
        }
        else {
            // this condition is to compare the tree node with the key,
            // if the tree node is smaller than the key, therefore
            // we have to check the right side of the tree, hence 
            // calling the function again but with index = 2*index+1
	    if (key_comp(*(bst->tree_nodes[index].key),*(key)) < 0) {
                bst_insert_helper(bst, key, data, 2*index + 1);
            }
	    // this condition is to compare the tree node with the key,
	    // if the tree node is larger than the key, therefore
	    // we have to check the left side of the tree, hence
	    // calling the function again but with index = 2*index
            else if ( key_comp(*(bst->tree_nodes[index].key), *(key)) > 0) {
                bst_insert_helper(bst, key, data, 2*index);
            }

	    // no condition is needed for the case where the key is 
	    // equal to the current tree node
        }
    }
}


// Input: ’bst’: a binary search tree
// ’key’: a pointer to Key
// ’data’: an integer
// Effect: ’data’ with ’key’ is inserted into ’bst’
// if ’key’ is already in ’bst’, do nothing
void bstree_insert(BStree bst, Key *key, int data) {
    bst_insert_helper(bst, key, data, 1);// call helper method to do the work
}

/**
 * @fn bst_traversal_helper 
 * this function does not return anything, it simply prints the nodes in order
 * if they exist, otherwise it doesn't,
 * and it prints in "in order"
 * takes the same paramters as bst_traversal but also keeps track of index
 */
void bst_traversal_helper(BStree bst, int index){// int goForward){
	if (bst->is_free[index] == '0'){
        bst_traversal_helper(bst, 2*index);
        print_node(bst->tree_nodes[index]);
        bst_traversal_helper(bst, 2*index + 1);
    }
}


// Input: ’bst’: a binary search tree
// Effect: print all the nodes in bst using in order traversal
void bstree_traversal(BStree bst) {
    int index = 1;//start searching at index 1
    bst_traversal_helper(bst, index); //calling the helper function that does the work
}

// Input: ’bst’: a binary search tree
// Effect: all memory used by bst are freed
void bstree_free(BStree bst) {
    //free array of nodes, then free array of is_free, then free bst
    free(tree_nodes);
    free(is_free);
    free(bst);}
