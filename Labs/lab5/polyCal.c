#include <stdio.h>
#include <math.h>

float powf(float x, float y);

void main(){
	float calVal =0;
	float val = 0;
	printf("please enter a value for x for the polynomial: 3x^5+2x^4-5x^3-x^2-7x-6 to be calculated:");
	scanf("%f", &val);
	
	calVal = (3*powf(val,5) + 2*powf(val,4) - 5*powf(val,3) -powf(val,2) - 7*val-6);
	printf("\nthe answer is %f\n", calVal);


}
